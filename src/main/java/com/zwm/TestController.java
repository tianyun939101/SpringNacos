package com.zwm;


import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope // 自动感知配置文件变化
@RequestMapping("test")
@RestController
public class TestController {

    // 从配置文件获取属性值
    @Value("${message:12}")
    private String message;

    @RequestMapping("message")
    public String message() {
        return message;
    }
}
